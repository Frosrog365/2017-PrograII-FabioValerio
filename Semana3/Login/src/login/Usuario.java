/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

/**
 *
 * @author Fabio
 */
public class Usuario {
    private String correo;
    private String usuario;
    private String contrasenna;
    private String nombre;
    private String apellidos;
    private String cedula;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public Usuario(String correo, String usuario, String contrasenna, String nombre, String apellidos, String cedula) {
        this.correo = correo;
        this.usuario = usuario;
        this.contrasenna = contrasenna;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.cedula = cedula;
    }

    @Override
    public String toString() {
        return "Usuario{" + "correo=" + correo + ", usuario=" + usuario + ", contrasenna=" + contrasenna + '}';
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public Usuario(String correo, String usuario, String contrasenna) {
        this.correo = correo;
        this.usuario = usuario;
        this.contrasenna = contrasenna;
    }

    public Usuario() {
    }
}
