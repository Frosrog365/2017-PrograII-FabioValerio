/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciointerfaz2;

import java.util.LinkedList;
import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.ListModel;

/**
 *
 * @author Fabio
 */
public class Logica {
    private LinkedList<Persona> personas;
    
    

    public LinkedList<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(LinkedList<Persona> personas) {
        this.personas = personas;
    }

    public Logica() {
        this.personas = new LinkedList<Persona>();
    }
    
    public void agregarPersonas(String nombre, String apellidos){
        Persona nuevaP = new Persona();
        nuevaP.setNombre(nombre);
        nuevaP.setApellido(apellidos);
        personas.add(nuevaP);
    }
}
