/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciointerfaz2;

import java.util.LinkedList;

/**
 *
 * @author Fabio
 */
public class Persona {
    private String nombre;
    private String apellido;
    private LinkedList<Perfil> perfiles;
    private Lugar[] lugaresFrecuentes;
    private Foto foto;

    public Persona() {
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }
    
    

    public void setPerfiles(LinkedList<Perfil> perfiles) {
        this.perfiles = perfiles;
    }

    public void setLugaresFrecuentes(Lugar[] lugaresFrecuentes) {
        this.lugaresFrecuentes = lugaresFrecuentes;
    }
    
    public void agregarLugarFrecuente(Lugar lugar){
        
    }
    
    public void agregarPerfil(){
        Perfil nuevo = new Perfil();
        perfiles.add(nuevo);
        
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public LinkedList<Perfil> getPerfiles() {
        return perfiles;
    }

    public Lugar[] getLugaresFrecuentes() {
        return lugaresFrecuentes;
    }

    public Foto getFoto() {
        return foto;
    }
}
