/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacioncoche;

/**
 *
 * @author Fabio
 */
public class Corazon {

    private int ritmo;

    public Corazon(int ritmo) {
        this.ritmo = ritmo;
    }

    public Corazon() {
    }

    public void setRitmo(int ritmo) {
        this.ritmo = ritmo;
    }

    /**
     * Método para cambiar el ritmo cardiaco
     *
     * @param valor el valor por el cual se va a sumar o restar el ritmo
     * @return el valor
     */
    public int cambiaRitmo(int valor) {
        return valor;
    }

    /**
     * Método que permite leer el ritmo cardiaco
     *
     * @return el ritmo cardiaco
     */
    public int leeRitmo() {
        return ritmo;
    }
}
