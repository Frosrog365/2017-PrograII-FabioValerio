/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaherencia;

/**
 *
 * @author estudiante
 */
public class Mamifero extends Animal{
    private String pelaje;
    private String extremidades;

    public Mamifero(String pelaje, String extremidades) {
        this.pelaje = pelaje;
        this.extremidades = extremidades;
    }

    public Mamifero() {
    }

    public String getPelaje() {
        return pelaje;
    }

    public void setPelaje(String pelaje) {
        this.pelaje = pelaje;
    }

    public String getExtremidades() {
        return extremidades;
    }

    public void setExtremidades(String extremidades) {
        this.extremidades = extremidades;
    }

    @Override
    public String toString() {
        return "Mamifero{" + "pelaje=" + pelaje + ", extremidades=" + extremidades + '}';
    }
    
    
}
