/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labanagrama.bo;

import java.util.LinkedList;
import java.util.Random;
import labanagrama.dao.PalabraDAO;
import labanagrama.entities.Palabra;

/**
 *
 * @author Fabio
 */
public class PalabraBO {

    private final PalabraDAO udao = new PalabraDAO();
    private int puntos;

    public PalabraBO() {
        this.puntos = 0;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    /**
     * Metodo para llamar al dao para cargar todo
     * @return la lista con las palabras
     */
    public LinkedList<Palabra> cargarTodo() {

        return udao.cargarTodo();

    }

    /**
     * Metodo para seleccionar una palabra random de la lista de palabras
     * @return Una palabra de todas de las de la lista
     */
    public String palabraToArray() {
        Random rand = new Random();
        int n = rand.nextInt(cargarTodo().size() - 1) + 0;
        String palabra = cargarTodo().get(n).getPalabra();
        return palabra;
    }

    /**
     * Metodo para desordenar una palabra
     * @param palabra la palabra que se va a desordenar
     * @return un String con la palabra desordenada
     */
    public String palabraDesordenada(String palabra) {
        Random rand = new Random();
        char a[] = palabra.toCharArray();
        int n = rand.nextInt(a.length - 1) + 0;

        for (int i = 0; i < a.length; i++) {
            int j = rand.nextInt(a.length);
            char temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }
        String nuevaPalabra = new String(a);
        for (int i = 0; i < cargarTodo().size(); i++) {
            if (nuevaPalabra.equals(cargarTodo().get(i).getPalabra())) {
                return palabraDesordenada(nuevaPalabra);
            }

        }
        return nuevaPalabra;
    }

    /**
     * Metodo para llamar al dao para que verifique si la palabra está desordenada
     * @param palabraIngresada
     * @return true o false
     */
    public boolean verificarPalabra(String palabraIngresada) {
        return udao.verificar(palabraIngresada);
    }

}
