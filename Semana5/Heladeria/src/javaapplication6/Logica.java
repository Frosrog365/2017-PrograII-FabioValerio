/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication6;

import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class Logica {

    LinkedList<User> usuarios;
    LinkedList<Articulo> articulos;

    public Logica() {
        this.usuarios = new LinkedList<>();
        this.articulos = new LinkedList<>();
    }

    public Logica(LinkedList<User> usuarios) {
        this.usuarios = usuarios;
    }

    public LinkedList<User> getUsuarios() {
        return usuarios;
    }

    public void setArticulos(LinkedList<Articulo> articulos) {
        this.articulos = articulos;
    }

    public LinkedList<Articulo> getArticulos() {
        return articulos;
    }

    public void setUsuarios(LinkedList<User> usuarios) {
        this.usuarios = usuarios;
    }

    public void agregarUsuarios(User nuevoUser) {
        usuarios.add(nuevoUser);
    }

    public void agregarArticulo(Articulo nuevoArt) {
        articulos.add(nuevoArt);
    }

    public User verificarUsuario(String correoUsuario, String contrasenna) {
        int error = 0;
        for (User usuario : usuarios) {
            if (contrasenna.equals(usuario.getContrasenna())
                    && correoUsuario.equals(usuario.getUsuario())) {
                return usuario;
            } else {
                error = 1;
            }

        }
        if (error == 1) {
            JOptionPane.showMessageDialog(null, "Contraseña Incorrecta", "Error en inicio de sesión", 0);
        } else if (error == 2) {
            JOptionPane.showMessageDialog(null, "Usuario no registrado", "Error en inicio de sesión", 0);
        }
        return null;
    }

    public boolean vender(Articulo art, int cantidad) {
        if (cantidad < art.getCantidad()) {
            art.setCantidad(art.getCantidad() - cantidad);
            return true;
        }
        return false;
    }

}
