/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacioncoche;

/**
 *
 * @author Fabio
 */
public class Coche {

    private Motor m;
    private Persona p;
    private boolean encendido;

    public Coche(Motor m, Persona p) {
        this.m = new Motor();
        this.p = p;
    }

    public Coche() {
    }
/**
 * Método para encender el carro
 * @return true que indica que el carro está encendido
 */
    public boolean enciende() {
        m.activa();
        return encendido = true;

    }

    public Coche(Motor m, Persona p, boolean encendido) {
        this.m = m;
        this.p = p;
        this.encendido = encendido;
    }
/**
 * Método para apagar el carro
 */
    public boolean apaga() {
        m.desactiva();
        return encendido = false;
    }
/**
 * Método para acelerar el carro
 * @return true que indica que el carro está acelerando
 */
    public boolean acelera() {
        
        m.setRevolucionesPorMinuto(m.cambiaRevolucionesPorMinuto(m.getRevolucionesPorMinuto() + 1000));
        System.out.println("Revoluciones"+m.getRevolucionesPorMinuto());
        return true;

    }
/**
 * Método para frenar el carro
 * @return true que indica que el carro está frenado
 */
    public boolean frena() {
       
        m.setRevolucionesPorMinuto(m.cambiaRevolucionesPorMinuto(m.getRevolucionesPorMinuto() - 1000));
        System.out.println("Revoluciones"+m.getRevolucionesPorMinuto());
        return true;
    }

    public Motor getM() {
        return m;
    }

    public void setM(Motor m) {
        this.m = m;
    }

    public Persona getP() {
        return p;
    }

    public void setP(Persona p) {
        this.p = p;
    }

    public boolean isEncendido() {
        return m.isActivo();
    }

    public void setEncendido(boolean encendido) {
        this.encendido = encendido;
    }
/**
 * Método para asiganar el conductor al carro
 * @return La persona conductora del carro
 */
    public Persona asignaConductor() {
        return p = new Persona();
    }
}
