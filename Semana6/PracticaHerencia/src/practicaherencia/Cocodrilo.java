/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaherencia;

/**
 *
 * @author estudiante
 */
public class Cocodrilo extends Reptil{
    private int cantidadDientes;

    public Cocodrilo() {
    }

    public Cocodrilo(int cantidadDientes) {
        this.cantidadDientes = cantidadDientes;
    }

    public int getCantidadDientes() {
        return cantidadDientes;
    }

    public void setCantidadDientes(int cantidadDientes) {
        this.cantidadDientes = cantidadDientes;
    }

    @Override
    public String toString() {
        return "Cocodrilo{" + "cantidadDientes=" + cantidadDientes + '}';
    }
    
}
