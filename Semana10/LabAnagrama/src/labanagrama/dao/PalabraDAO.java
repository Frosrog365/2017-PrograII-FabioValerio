/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labanagrama.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import labanagrama.entities.MiError;
import labanagrama.entities.Palabra;

/**
 *
 * @author estudiante
 */
public class PalabraDAO {

    private Palabra cargarPalabra(ResultSet rs) throws SQLException {
        Palabra u = new Palabra();
        u.setPalabra(rs.getString("palabra"));
        return u;
    }

    /**
     * Metodo para cargar las palabras de la db a una lista
     * @return una lista con las palabras de la db
     */
    public LinkedList<Palabra> cargarTodo() {
        LinkedList<Palabra> palabras = new LinkedList<>();

        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from palabra";
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                palabras.add(cargarPalabra(rs));
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new MiError("Problemas al cargar las palabras, favor intente nuevamente");
        }

        return palabras;
    }
/**
 * Metodo para verificar si la palabra que el usuario inserte, existe dentro de la base de datos
 * @param palabraInsertada La palabra que el usuario inserta
 * @return true o false
 */
    public boolean verificar(String palabraInsertada) {
        try (Connection con = Conexion.getConexion()) {
            String sql = "select * from palabra where palabra = ?";
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setString(1, palabraInsertada);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new MiError("Problemas al cargar las palabras, favor intente nuevamente");
        }

        return false;
    }
}
