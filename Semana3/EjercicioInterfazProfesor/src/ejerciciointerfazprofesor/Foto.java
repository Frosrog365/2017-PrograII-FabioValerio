/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciointerfazprofesor;

/**
 *
 * @author estudiante
 */
public class Foto {

    private String ruta;
    private String tipoArchivo;
    private float tamanno;

    public Foto() {
    }

    public Foto(String ruta, String tipoArchivo, float tamanno) {
        this.ruta = ruta;
        this.tipoArchivo = tipoArchivo;
        this.tamanno = tamanno;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public float getTamanno() {
        return tamanno;
    }

    public void setTamanno(float tamanno) {
        this.tamanno = tamanno;
    }

    @Override
    public String toString() {
        return "Foto{" + "ruta=" + ruta + ", tipoArchivo=" + tipoArchivo + ", tamanno=" + tamanno + '}';
    }
    
    
}
