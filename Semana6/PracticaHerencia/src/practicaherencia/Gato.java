/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaherencia;

/**
 *
 * @author estudiante
 */
public class Gato extends Mamifero{
    private String frecMaullido;
    private boolean tienePedigree;

    public Gato(String frecMaullido, boolean tienePedigree) {
        this.frecMaullido = frecMaullido;
        this.tienePedigree = tienePedigree;
    }

    public Gato() {
    }

    public String getFrecMaullido() {
        return frecMaullido;
    }

    public void setFrecMaullido(String frecMaullido) {
        this.frecMaullido = frecMaullido;
    }

    public boolean isTienePedigree() {
        return tienePedigree;
    }

    public void setTienePedigree(boolean tienePedigree) {
        this.tienePedigree = tienePedigree;
    }

    @Override
    public String toString() {
        return "Gato{" + "frecMaullido=" + frecMaullido + ", tienePedigree=" + tienePedigree + '}';
    }
}
