/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herenciavehiculo;

/**
 *
 * @author Fabio Valerio
 */
public class Camion extends Vehiculo{
    int CargaToneladas;

    public Camion(int CargaToneladas) {
        this.CargaToneladas = CargaToneladas;
    }

    public Camion() {
    }

    public int getCargaToneladas() {
        return CargaToneladas;
    }

    public void setCargaToneladas(int CargaToneladas) {
        this.CargaToneladas = CargaToneladas;
    }

    @Override
    public String toString() {
        return "Camion{" + "CargaToneladas=" + CargaToneladas + '}';
    }
    
    
}
