/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciointerfazprofesor;

import com.sun.corba.se.spi.orbutil.proxy.LinkedInvocationHandler;
import java.util.LinkedList;

/**
 *
 * @author estudiante
 */
public class Persona {

    private int id;
    private String nombre;
    private String apellido;
    private LinkedList<Perfil> perfiles;
    private LinkedList<Lugar> lugares;
    private Foto foto;

    public Persona() {
        perfiles = new LinkedList<>();
        lugares = new LinkedList<>();
    }

    public Persona(int id, String nombre, String apellido, Foto foto) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        perfiles = new LinkedList<>();
        lugares = new LinkedList<>();
        this.foto = foto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public LinkedList<Perfil> getPerfiles() {
        return perfiles;
    }

    public void setPerfiles(LinkedList<Perfil> perfiles) {
        this.perfiles = perfiles;
    }

    public LinkedList<Lugar> getLugares() {
        return lugares;
    }

    public void setLugares(LinkedList<Lugar> lugares) {
        this.lugares = lugares;
    }

    public Foto getFoto() {
        return foto;
    }

    public void setFoto(Foto foto) {
        this.foto = foto;
    }
    /**
     * Agrega un lugar frecuente a la persona
     * @param l lugar que se desea agregar
     */
    public void agregarLugar(Lugar l){
        lugares.add(l);
    }
    
    /**
     * Crea un nuevo perfil para la persona
     * @param tipoPerfil int Tipo del perfil
     * @param desc String descripcion del perfil
     */
    public void agregarPerfil(int tipoPerfil, String desc){
        perfiles.add(new Perfil(tipoPerfil, desc));
    }
}
