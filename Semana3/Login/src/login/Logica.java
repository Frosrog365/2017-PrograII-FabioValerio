/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class Logica {

    private LinkedList<Usuario> usuarios;

    @Override

    public String toString() {
        return "Logica{" + "usuarios=" + usuarios + '}';
    }

    public Logica(LinkedList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public Logica() {
        usuarios = new LinkedList<>();
    }

    public LinkedList<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(LinkedList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public void agregarUsuario(Usuario u) {
        this.usuarios.add(u);
    }

    public Usuario verificarUsuario(String correoUsuario, String contrasenna) {
        int error = 0;
        for (Usuario usuario : usuarios) {
            if ((correoUsuario.equals(usuario.getCorreo()) || correoUsuario.equals(usuario.getUsuario()))
                    && contrasenna.equals(usuario.getContrasenna())) {
                return usuario;
            } else if ((correoUsuario.equals(usuario.getCorreo()) || correoUsuario.equals(usuario.getUsuario()))
                    && (contrasenna == null ? (usuario.getContrasenna()) != null : !contrasenna.equals(usuario.getContrasenna()))) {

                error = 1;
            } else if ((correoUsuario == null ? usuario.getCorreo() != null : !correoUsuario.equals(usuario.getCorreo())) || (correoUsuario == null ? usuario.getUsuario() != null : !correoUsuario.equals(usuario.getUsuario()))) {

                error = 2;
            }
        }
        if (error == 1) {
            JOptionPane.showMessageDialog(null, "Contraseña Incorrecta", "Error en inicio de sesión", 0);
        } else if (error == 2) {
            JOptionPane.showMessageDialog(null, "Usuario no registrado", "Error en inicio de sesión", 0);
        }
        return null;
    }

    public void modificarUsuario(Usuario elegido, String nombre, String apellidos, String cedula, String correo, String usuario, String uUsuario, String uContrasenna) {
        elegido = verificarUsuario(uUsuario, uContrasenna);
        elegido.setNombre(nombre);
        elegido.setApellidos(apellidos);
        elegido.setCedula(cedula);
        elegido.setCorreo(correo);
        elegido.setUsuario(usuario);
    }
}
