/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaherencia;

/**
 *
 * @author estudiante
 */
public class Reptil extends Animal{
    private String cantidadHuevos;
    private boolean tieneVeneno;

    public Reptil() {
    }

    public Reptil(String cantidadHuevos, boolean tieneVeneno) {
        this.cantidadHuevos = cantidadHuevos;
        this.tieneVeneno = tieneVeneno;
    }

    public String getCantidadHuevos() {
        return cantidadHuevos;
    }

    public void setCantidadHuevos(String cantidadHuevos) {
        this.cantidadHuevos = cantidadHuevos;
    }

    public boolean isTieneVeneno() {
        return tieneVeneno;
    }

    public void setTieneVeneno(boolean tieneVeneno) {
        this.tieneVeneno = tieneVeneno;
    }

}

