/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herenciavehiculo;

/**
 *
 * @author Fabio Valerio
 */
public class CamionCompartimientos extends Vehiculo {

    double cargaMaxima;
    int cantCompartimientos;

    public CamionCompartimientos() {
    }

    public CamionCompartimientos(double cargaMaxima, int cantCompartimientos) {
        this.cargaMaxima = cargaMaxima;
        this.cantCompartimientos = cantCompartimientos;
    }

    public double getCargaMaxima() {
        return cargaMaxima;
    }

    public void setCargaMaxima(double cargaMaxima) {
        this.cargaMaxima = cargaMaxima;
    }

    public int getCantCompartimientos() {
        return cantCompartimientos;
    }

    public void setCantCompartimientos(int cantCompartimientos) {
        this.cantCompartimientos = cantCompartimientos;
    }
    
    public double capCargaPorCompart(){
        return cargaMaxima / cantCompartimientos;
    }

    @Override
    public String toString() {
        return marca + " " + cantCompartimientos;
    }

}
