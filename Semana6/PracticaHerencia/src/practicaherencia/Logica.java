/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaherencia;

import java.util.LinkedList;

/**
 *
 * @author estudiante
 */
public class Logica {

    private LinkedList<Animal> animales;

    public Logica() {
        this.animales = new LinkedList<>();
    }

    public LinkedList<Animal> getAnimales() {
        return animales;
    }

    public void setAnimales(LinkedList<Animal> animales) {
        this.animales = animales;
    }

    @Override
    public String toString() {
        return "Logica{" + "animales=" + animales + '}';
    }
    
    public void agregarAnimales(Animal a){
        animales.add(a);
    }

}
