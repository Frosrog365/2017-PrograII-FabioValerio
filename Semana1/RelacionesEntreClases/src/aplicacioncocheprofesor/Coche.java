/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacioncocheprofesor;

/**
 *
 * @author estudiante
 */
public class Coche {

    private Motor motor;
    private Persona conductor;

    public Coche(Motor motor) {
        this.motor = motor;
    }

    public void setConductor(Persona conductor) {
        this.conductor = conductor;
    }

    public void enciende() {
        if (motor != null && !motor.isActivo()) {
            motor.activar();
            motor.cambiaRpm(1000);
        }
    }

    public void apaga() {
        if (motor != null && motor.isActivo()) {
            motor.cambiaRpm(0);
            motor.desactivar();
        }
    }
    
    public void acelera(){
         if (motor != null && motor.isActivo()) {
             motor.cambiaRpm(500);
             
         }
    }

}
