/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaherencia;

/**
 *
 * @author estudiante
 */
public class Animal {
    private String tamaño;
    private String edad;
    private String clase;

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public Animal(String tamaño, String edad) {
        this.tamaño = tamaño;
        this.edad = edad;
    }

    public Animal() {
    }

    public String getTamaño() {
        return tamaño;
    }

    public void setTamaño(String tamaño) {
        this.tamaño = tamaño;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        return "Animal{" + "tama\u00f1o=" + tamaño + ", edad=" + edad + '}';
    }
    
    
}
