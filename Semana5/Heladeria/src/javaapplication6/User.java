/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication6;

/**
 *
 * @author estudiante
 */
public class User extends Persona {

    String usuario;
    int rol;
    String contrasenna;

    public User() {
    }

    public User(String usuario, int rol, String contrasenna) {
        this.usuario = usuario;
        this.rol = rol;
        this.contrasenna = contrasenna;
    }

    public User(String usuario, int rol, String contrasenna, String nombre, String cedula, String telefono, String direccion) {
        super(nombre, cedula, telefono, direccion);
        this.usuario = usuario;
        this.rol = rol;
        this.contrasenna = contrasenna;
    }

    

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public int getRol() {
        return rol;
    }

    public void setRol(int rol) {
        this.rol = rol;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    @Override
    public String toString() {
        return "Admin{" + "usuario=" + usuario + ", rol=" + rol + ", contrasenna=" + contrasenna + '}';
    }
    
    public void RegistrarInventario(){
        
    }
    
    public void consultarInventario(){
        
    }
    public void vender(){
        
    }
}
