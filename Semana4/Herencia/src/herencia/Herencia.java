/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author Fabio Valerio
 */
public class Herencia {

    public void mostrar() {
        Persona p = new Persona();
        p.setCedula("204560123");
        p.setNombre("Luis");

        Alumno a = new Alumno();
        a.setNombre("Fabio Valerio");
        a.setCedula("207420324");
        a.setCarrera("ISW");
        System.out.println(a);
    }

}
