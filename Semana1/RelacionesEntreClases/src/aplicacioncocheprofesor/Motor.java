/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacioncocheprofesor;

/**
 *
 * @author estudiante
 */
public class Motor {
    private int rpm;
    private boolean activo;

    public Motor() {
    }

    public void cambiaRpm(int rpm) {
        this.rpm = rpm;
    }

    public boolean isActivo() {
        return activo;
    }
    public void activar(){
        activo = true;
    }
    public void desactivar(){
        activo = false;
    }
    
    
}
