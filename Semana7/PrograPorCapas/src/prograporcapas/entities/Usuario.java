/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prograporcapas.entities;

/**
 *
 * @author estudiante
 */
public class Usuario {
    public int id;
    public String email;
    public String contrasenna;
    private int telefono;

    public Usuario() {
    }

    public Usuario(int id, String email, String contrasenna, int telefono) {
        this.id = id;
        this.email = email;
        this.contrasenna = contrasenna;
        this.telefono = telefono;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContrasenna() {
        return contrasenna;
    }

    public void setContrasenna(String contrasenna) {
        this.contrasenna = contrasenna;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getTelefono() {
        return telefono;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", email=" + email + ", contrasenna=" + contrasenna + ", telefono=" + telefono + '}';
    }

   
    
    
    
    
}
