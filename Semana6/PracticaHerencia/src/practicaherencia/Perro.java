/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaherencia;

/**
 *
 * @author estudiante
 */
public class Perro extends Mamifero{
    private boolean estaEntrenado;
    private boolean tienePedigree;

    public Perro() {
    }

    public Perro(boolean estaEntrenado, boolean tienePedigree) {
        this.estaEntrenado = estaEntrenado;
        this.tienePedigree = tienePedigree;
    }

    public boolean isEstaEntrenado() {
        return estaEntrenado;
    }

    public void setEstaEntrenado(boolean estaEntrenado) {
        this.estaEntrenado = estaEntrenado;
    }

    public boolean isTienePedigree() {
        return tienePedigree;
    }

    public void setTienePedigree(boolean tienePedigree) {
        this.tienePedigree = tienePedigree;
    }

    @Override
    public String toString() {
        return "Perro{" + "estaEntrenado=" + estaEntrenado + ", tienePedigree=" + tienePedigree + '}';
    }
    
    
}
