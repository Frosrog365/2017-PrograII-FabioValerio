/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herenciavehiculo;

/**
 *
 * @author Fabio Valerio
 */
public class Autobus extends Vehiculo{
    int CantAsientos;

    public Autobus() {
    }

    public Autobus(int CantAsientos) {
        this.CantAsientos = CantAsientos;
    }

    public int getCantAsientos() {
        return CantAsientos;
    }

    public void setCantAsientos(int CantAsientos) {
        this.CantAsientos = CantAsientos;
    }

    @Override
    public String toString() {
        return "Autobus{" + "CantAsientos=" + CantAsientos + '}';
    }
    
}
