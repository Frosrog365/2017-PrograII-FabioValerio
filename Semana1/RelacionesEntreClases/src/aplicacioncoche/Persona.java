/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacioncoche;

import javax.swing.JOptionPane;

/**
 *
 * @author Fabio
 */
public class Persona {

    private String nombre;
    private Corazon c;
    private Coche carro;
    private boolean vive;

    public Persona(String nombre) {
        this.nombre = nombre;
        this.c = new Corazon(60);
        this.vive = true;
    }

    public String getNombre() {
        return nombre;
    }

    public Persona() {
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Corazon getC() {
        return c;
    }

    public void setC(Corazon c) {
        this.c = c;
    }

    public Coche getCarro() {
        return carro;
    }

    public void setCarro(Coche carro) {
        this.carro = carro;
    }

    public void asignaCoche(Coche c) {
        carro = new Coche();
        carro.setM(new Motor(0, true));
    }

    public boolean isVive() {
        return vive;
    }

    public void setVive(boolean vive) {
        this.vive = vive;
    }

    public void emociona() {
        c.setRitmo(c.cambiaRitmo(c.leeRitmo() + 10));
        System.out.println("Ritmo Cardiaco: " + c.leeRitmo());
    }

    public void tranquiliza() {
        c.setRitmo(c.cambiaRitmo(c.leeRitmo() - 10));
        System.out.println("Ritmo Cardiaco: " + c.leeRitmo());
    }

    public void viaja() {
        int opcion = Integer.parseInt(JOptionPane.showInputDialog("1. Encender el carro\n"));
        if (opcion == 1) {
            carro.enciende();
        } else if (opcion == 2) {
            System.exit(0);
        }
        while (carro.isEncendido()) {
            int opcion2 = Integer.parseInt(JOptionPane.showInputDialog("1. Acelerar\n"
                    + "2. Frenar\n"
                    + "3. Apagar el carro"));
            if (opcion2 == 1) {
                carro.acelera();
                emociona();
                if(c.leeRitmo()>200){
                    System.out.println("Le dió un paro");
                }
                if(carro.getM().getRevolucionesPorMinuto()>7000){
                    System.out.println("Fundió el motor");
                    
                }
            }
            if (opcion2 == 2) {
                carro.frena();
                tranquiliza();
            } else if (opcion == 3) {
                carro.apaga();
            }
        }

    }

}
