/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciointerfazprofesor;

import java.util.LinkedList;

/**
 *
 * @author estudiante
 */
public class Logica {

    private LinkedList<Persona> personas;
    private LinkedList<Lugar> lugares;

    public Logica() {
        personas = new LinkedList<>();
        lugares = new LinkedList<>();
    }

    public void agregarLugar(Lugar l) {
        lugares.add(l);
    }

    public LinkedList<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(LinkedList<Persona> personas) {
        this.personas = personas;
    }

    public LinkedList<Lugar> getLugares() {
        return lugares;
    }

    public void setLugares(LinkedList<Lugar> lugares) {
        this.lugares = lugares;
    }
    

}
