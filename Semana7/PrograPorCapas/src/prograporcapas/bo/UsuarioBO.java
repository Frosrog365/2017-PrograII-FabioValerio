package prograporcapas.bo;

import prograporcapas.entities.Usuario;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author estudiante
 */
public class UsuarioBO {
// Validar el usuario, campos requeridos, igualdad de contraseñas,
    // Formatos de fechas, cedulas, numeros, etc.

    public boolean registrar(Usuario usuario, String reContra) {
        if (usuario.getEmail().isEmpty()) {
            throw new RuntimeException("Correo Requerido");
        }
        if (usuario.getContrasenna().isEmpty()) {
            throw new RuntimeException("Contraseña Requerida");
        }
        if (!usuario.getContrasenna().equals(reContra)) {
            throw new RuntimeException("Contraseñas no coinciden");
        }
        return true;
    }

}
