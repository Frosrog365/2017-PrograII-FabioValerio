/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package labanagrama.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import labanagrama.entities.MiError;

/**
 *
 * @author ALLAN
 */
public class Conexion {

    public static final String DRIVER = "jdbc:postgresql://";
    public static final String SERVER = "localhost:5433/";
    public static final String DB = "anagrama";
    public static final String USER = "postgres";
    public static final String PASS = "sa123456";

    public static Connection getConexion() {
        Connection conn = null;
        String url = DRIVER + SERVER + DB;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, USER, PASS);
        } catch (ClassNotFoundException ex) {
            throw new MiError("Falta el driver de base de datos");
        } catch (SQLException ex) {
            throw new MiError("Problemas al realizar la conexión\n" + ex.getMessage());
        }
        return conn;
    }
}
