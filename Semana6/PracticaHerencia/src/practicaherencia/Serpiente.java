/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaherencia;

/**
 *
 * @author estudiante
 */
public class Serpiente extends Reptil{
    private int largoColmillos;

    public Serpiente() {
    }

    public Serpiente(int largoColmillos) {
        this.largoColmillos = largoColmillos;
    }

    public int getLargoColmillos() {
        return largoColmillos;
    }

    public void setLargoColmillos(int largoColmillos) {
        this.largoColmillos = largoColmillos;
    }

    @Override
    public String toString() {
        return "Serpiente{" + "largoColmillos=" + largoColmillos + '}';
    }
    
}
