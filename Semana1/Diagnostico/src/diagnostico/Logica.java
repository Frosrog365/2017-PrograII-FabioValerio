/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

import java.util.LinkedList;
import javax.swing.JOptionPane;

/**
 *
 * @author estudiante
 */
public class Logica {

    private String frase;
    private LinkedList<Character> arregloPalabra;
    private LinkedList<Character> arregloEquisyOs;
    private LinkedList<LinkedList> matriz;
    private String arreglo1;
    private String arreglo2;
    private String matrizStr;

    /**
     * Metodo para imprimir la matriz
     * @return String con la matriz
     */
    public String getMatriz() {
        matrizStr="";
        for (int i = 0; i < matriz.size(); i++) {
            matrizStr+=matriz.get(i)+"\n";
        }
        matrizStr+="\b";
        return matrizStr;

    }

    public Logica() {
    }

    /**
     * Metodo para que el usuario digite la oración
     */
    public void digitarOracion() {
        frase = JOptionPane.showInputDialog("Escriba la frase/oración");
        frase = frase.toLowerCase();
    }

    /**
     * Metodo para que se divida la frase en letras
     * @return String con el arreglo de letras divididas
     */
    public String arregloFrase() {
        arreglo1 = "";
        arregloPalabra = new LinkedList<>();
        for (int i = 0; i < frase.length(); i++) {

            arregloPalabra.add(frase.charAt(i));
        }

        for (int i = 0; i < arregloPalabra.size(); i++) {
            arreglo1 += arregloPalabra.get(i) + "|";

        }
        arreglo1+="\b";
        return arreglo1;

    }

    /**
     * Metodo para verificar que las letras insertadas son vocales si lo son se inserta una X, o una O de lo contrario
     * @return String con el arreglo de vocales
     */
    public String arregloVocales() {
        arreglo2 = "";
        arregloEquisyOs = new LinkedList<>();
        for (int i = 0; i < frase.length(); i++) {
            if (frase.charAt(i) == 'a' || frase.charAt(i) == 'e' || frase.charAt(i) == 'i' || frase.charAt(i) == 'o' || frase.charAt(i) == 'u') {
                arregloEquisyOs.add('X');
            } else if (frase.charAt(i) == ' ') {
                arregloEquisyOs.add(' ');
            } else {
                arregloEquisyOs.add('O');
            }
        }
        for (int i = 0; i < arregloEquisyOs.size(); i++) {
            arreglo2 += arregloEquisyOs.get(i) + "|";
        }
        arreglo2+="\b";
        return arreglo2;
    }

    /**
     * Genera la matriz
     */
    public void generarMatriz() {
        matriz = new LinkedList<>();
        matriz.add(arregloPalabra);
        matriz.add(arregloEquisyOs);
    }

}
