/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication6;

/**
 *
 * @author Fabio_2
 */
public class Articulo {

    private String descripcion;
    private int cantidad;
    private int precio;
    private String tipo;

    public Articulo() {
    }

    public Articulo(String descripcion, int cantidad, int precio, String tipo) {
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precio = precio;
        this.tipo = tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
