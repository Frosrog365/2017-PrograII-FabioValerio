/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacioncoche;

/**
 *
 * @author Fabio
 */
public class Motor {

    private int revolucionesPorMinuto;
    private boolean activo;

    public int getRevolucionesPorMinuto() {
        return revolucionesPorMinuto;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public void setRevolucionesPorMinuto(int revolucionesPorMinuto) {
        this.revolucionesPorMinuto = revolucionesPorMinuto;
    }

    public Motor(int revolucionesPorMinuto, boolean activo) {
        this.revolucionesPorMinuto = revolucionesPorMinuto;
        this.activo = activo;
    }

    public Motor() {
    }

    public int cambiaRevolucionesPorMinuto(int rmp) {
        return rmp;
    }

    public boolean estaActivo() {
        return activo;
    }

    public boolean activa() {
        return activo = true;
    }

    public boolean desactiva() {
        return activo = false;
    }

}
