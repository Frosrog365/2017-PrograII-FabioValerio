/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaherencia;

/**
 *
 * @author estudiante
 */
public class Mujer extends Humano{
    private boolean estaEmbarazada;

    public Mujer(String cedula, String nombre) {
        super(cedula, nombre);
    }

    public Mujer(boolean estaEmbarazada, String cedula, String nombre) {
        super(cedula, nombre);
        this.estaEmbarazada = estaEmbarazada;
    }

    public boolean isEstaEmbarazada() {
        return estaEmbarazada;
    }

    public void setEstaEmbarazada(boolean estaEmbarazada) {
        this.estaEmbarazada = estaEmbarazada;
    }

    @Override
    public String toString() {
        return "Mujer{" + "estaEmbarazada=" + estaEmbarazada + '}';
    }


    
    
    
}
