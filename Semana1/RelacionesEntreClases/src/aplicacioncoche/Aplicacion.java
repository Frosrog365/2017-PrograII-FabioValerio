/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicacioncoche;

/**
 *
 * @author Fabio
 */
public class Aplicacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Persona p = new Persona("Fabio Valerio");
        Coche c = new Coche();
        p.asignaCoche(c);
        c.asignaConductor();
        p.viaja();

    }

}
