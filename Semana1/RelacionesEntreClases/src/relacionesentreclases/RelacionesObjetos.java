/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relacionesentreclases;

import java.util.Date;

/**
 *
 * @author Fabio
 */
public class RelacionesObjetos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cliente cl = new Cliente("Fabio", "Valerio Acuña", "500 metros este Banco Nacional", "Aguas Zarcas", new Date());
        Cuenta cu = new Cuenta(123123121, 0.03, cl);
        System.out.println(cu.getTitular().nombreCompleto());
        System.out.println(cu.getTitular().direccionCompleta());
        cu.ingreso(1000);
        System.out.println(cu.enRojos());
        Poligono p = new Poligono();
        p.agregarSegmento(new Segmento(10));
        p.agregarSegmento(new Segmento(11));
        p.agregarSegmento(new Segmento(12));
        p.agregarSegmento(new Segmento(13));
        System.out.println(p);
       

    }

}
